import base64
import requests
import io
import os
import json
import sys
from PIL import Image, ImageDraw
import re
import datetime
#from datetime import date
from difflib import get_close_matches
import dateparser
from countrycodes import codes
import logging
import logging.handlers
from os.path import expanduser
import traceback
home=expanduser('~')
LOG_FILENAME = home+'/'+'detail.log'

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s - %(levelname)s - %(message)s",
    handlers=[
        logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=30000000, backupCount=100),
        logging.StreamHandler(sys.stdout)
    ])
my_logger = logging.getLogger('detailslogger')


def detect_text(image_file):
    try:
        loss=[]
        exact_length=[]
        two_lines_mrz=[]
        my_logger.info("detect_text initiated:= {}".format(image_file[1:10]))
        url = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyAOztXTencncNtoRENa1E3I0jdgTR7IfL0'
        header = {'Content-Type': 'application/json'}
        body = {
            'requests': [{
                'image': {
                    'content': image_file,
                },
                'features': [{
                    'type': 'DOCUMENT_TEXT_DETECTION',
                    'maxResults': 100,
                }]

            }]
        }
        response = requests.post(url, headers=header, json=body).json()
        text = response['responses'][0]['textAnnotations'][0]['description'] if len(response['responses'][0]) > 0 else ''
        full_text=str(text).split('\n')
        my_logger.info("text from visionapi: ={}".format(text))   
        reverse_text=full_text[::-1]
        ca=re.sub(r'\s+', '',str(reverse_text[2]))
        loss.append(ca)
        a=re.sub(r'\s+', '',str(reverse_text[1]))
        print(text)
        loss.append(a)
        for x in reverse_text:
            if (len(x)>=25):
                exact_length.append(x)

        line_first = re.sub(r'\s+', '',str(exact_length[1]))
        two_lines_mrz.append(line_first)
        line_second = re.sub(r'\s+', '',str(exact_length[0]))
        two_lines_mrz.append(line_second)

        first=two_lines_mrz[0]
        second =two_lines_mrz[1]
        my_logger.info("two lines  mrzcode: ={}".format(two_lines_mrz))



        if(first[0]=='P'):
            my_logger.info("passport  content: ={}".format(two_lines_mrz))
            passport_type=('\n'.join(tuple(loss)))

            type=first[0]
            date_issue=re.findall(r'\s([0-9][0-9] [a-zA-Z]+ \d{4}|\d{2}/\d{2}/\d{4}|\d{2}.\d{2}.\d{4}|\d{2} \w+/\w+ \d{4}|\d{2} \d{2} \d{4}|\d{2}-d{2}-d{4}|\d{2} \w+ /\w+ \d{2}|\d{2} \w+ \d{2}|\d{2} \w+/\w+ \d{2}|\d{2} \w+ \w+ \d{2}|\d{2}-\w+-\d{4}|\d{2} \w+\/ \w+ \d{4}|\d{2} \d{2}\. \d{4})',text)
            #print("date of birth:",date_issue)
            static  = [' ','.','/','-']
            dates=[]
            for x in date_issue:
                    for y in static:
                            if y in (x):
                                if len(x)>7:
                                    dates.append(x)

            res = []
            [res.append(x) for x in dates if x not in res]
            my_logger.info("extracted dates: {}".format(dates))
            parsed_issue=dateparser.parse(res[1],settings={'DATE_ORDER': 'YMD'})
            if parsed_issue==None:
                Date_of_issue=' '
            else:
                issue_join=str((parsed_issue).date())
                Date_of_issue=issue_join

            #Date_of_issue=res[1]
            country_code=re.sub('\ |\?|\.|\!|\/|\;|\:|\<', ' ', first[2:5])

            name_with_symbols=first[5:45]

            fullname = name_with_symbols.strip('<')
            name_spliting = fullname.split('<<')
            surname = re.sub('\ |\?|\.|\!|\/|\;|\:|\<|\>|«', ' ', name_spliting[0])

            if (len(name_spliting)==2):
                mrx=re.sub('\ |\?|\.|\!|\/|\;|\:|\<|\>|«', ' ', name_spliting[1])
                givenname=mrx
                    #print("given name:",givenname)
            else:
                givenname = ''

            document_no=second[0:9]
            passport_no=re.sub(r'[^\w]', ' ',document_no)

            nationality=re.sub('\ |\?|\.|\!|\/|\;|\:|\<', ' ', second[10:13])

            birthdate=second[13:19]
            birth_joindate = '/'.join([birthdate[:2],birthdate[2:4],birthdate[4:]])
            parsed_birth=dateparser.parse(birth_joindate,settings={'DATE_ORDER': 'YMD'})
            #print(parsed_birth)
            if parsed_birth==None:
                date_of_birth =''
            else:
                date_of_birth = str((parsed_birth).date())
                year = date_of_birth[0:4]

                present_date=datetime.datetime.now()
                present_year=present_date.year
                if str(present_year)<year:
                    two=date_of_birth[0:2]
                    remain=date_of_birth[2:]
                    full=two.replace(str(20),str(19))
                    date_of_birth=full+remain

            sex=second[20]

            expiry_date=second[21:27]
            expiry_joindate ='/'.join([expiry_date[:2],expiry_date[2:4],expiry_date[4:]])
            parsed_expiry=dateparser.parse(expiry_joindate,settings={'DATE_ORDER': 'YMD'})
            if parsed_expiry==None:
                date_of_expiry =' '
            else:
                date_of_expiry = str((parsed_expiry).date())
            if Date_of_issue == date_of_expiry or Date_of_issue == date_of_birth:
                Date_of_issue= ' '
            if Date_of_issue !=' ':
                Date_of_issue=Date_of_issue[8:]+'/'+Date_of_issue[5:7]+'/'+Date_of_issue[0:4]
            if date_of_birth !='':
                date_of_birth=date_of_birth[8:]+'/'+date_of_birth[5:7]+'/'+date_of_birth[0:4]
            if date_of_expiry !=' ':
                date_of_expiry =date_of_expiry[8:]+'/'+date_of_expiry[5:7]+'/'+date_of_expiry[0:4]
            country_code2=''
            country_long=''
            nationality_code2=''
            nationality_long=''
            for x in codes:
                for key,values in x.items():
                    if values == country_code:
                        country_long = x["Full_Name"]
                        country_code2 = x["ISO_2_Code"]
                        
                    if values == nationality:
                        nationality_long = x["Full_Name"]
                        nationality_code2 = x["ISO_2_Code"]
            if  sex!='M' or sex!='F':
                sex = ' ' 
            data={"IDType":"passport","Country":country_code2,"FirstName":surname,"MiddleName":"","LastName":givenname,"IssueDate":Date_of_issue,"PassportNumber":passport_no,"Nationality":nationality_code2,"BirthDate":date_of_birth,"Sex":sex,"ExpirationDate":date_of_expiry,"Address1":" ","Address2":" ","Address3":" ","CountryLong":country_long,"NationalityLong":nationality_long,"PersonalNumber":" ","PlaceOfBirth":" "}
            my_logger.info("details-extracted:{}".format(data))
            details={"IDType":"passport","PassportNumber":"INNG778893","FirstName":"ram","MiddleName":"","LastName":"raghu","Sex":"M","BirthDate":"23/09/1994","PlaceOfBirth":"","Country":"IND","CountryLong":"India","Nationality":"IND","NationalityLong":"India","ExpirationDate":"23/09/2030","Address1":"","Address2":"","Address3":"","IssueDate":"23/09/2018","PersonalNumber":""}
            details={"type":"PASSPORT","data":data}
        # print("person_passport_details:",data)
            return data
        elif(first[0]=='V'):
            my_logger.info("visa-content:{}".format(two_lines_mrz))
            visa_type=('\n'.join(tuple(loss)))

            type=first[0]
            issuingcountry=re.sub('\ |\?|\.|\!|\/|\;|\:|\<', ' ', first[2:5])
            if (first[1].isalpha()):
                date_issue=re.findall(r'\s([0-9][0-9] [a-zA-Z]+ \d{4}|\d{2}/\d{2}/\d{4}|\d{2}.\d{2}.\d{4}|\d{2} \w+/\w+ \d{4}|\d{2} \d{2} \d{4}|\d{2}-d{2}-d{4}|\d{2} \w+ /\w+ \d{2}|\d{2} \w+/\w+ \d{4} \w+|\d{2}-\w+-\d{4}|\d{2} \w+\d{4})',text)
                print("date of issue:",date_issue)
                static  = [' ','.','/','-']
                dates=[]
                for x in date_issue:
                        for y in static:
                                if y in (x):
                                    if len(x)>7:
                                        dates.append(x)
                my_logger.info("visa_dates_extracted: {}".format(dates))
                issue_date = []
                [issue_date.append(x) for x in dates if x not in issue_date]
                #print("date of issue:",issue_date)
                min_year = issue_date[0][6:10]
                if len(issue_date)>1:
                    min_year = min(issue_date[0][6:10], issue_date[1][6:10])
                type_of_visa = first[1]
                my_logger.info("visa_issuedates_extracted: {}".format(issue_date))
                for x in issue_date:
                    if min_year in x:
                        Date=x
                        parsed_issue=dateparser.parse(Date,settings={'DATE_ORDER': 'DMY'})
                        if parsed_issue==None:
                            Date_of_issue=' '
                        else:
                            issue_join=str((parsed_issue).date())
                            Date_of_issue=issue_join
                        #print("great value:",x)
            elif (first[1]=='<'):
                date_issue=re.findall(r'\s([0-9][0-9] [a-zA-Z]+ \d{4}|\d{2}/\d{2}/\d{4}|\d{2}.\d{2}.\d{4}|\d{2} \w+/\w+ \d{4}|\d{2} \d{2} \d{4}|\d{2}-d{2}-d{4}|\d{2} \w+ /\w+ \d{2}|\d{2} \w+/\w+ \d{4} \w+|\d{2}-\w+-\d{4})',text)
                static  = [' ','.','/','-']
                dates=[]
                for x in date_issue:
                        for y in static:
                                if y in (x):
                                    if len(x)>7:
                                        dates.append(x)

                issue_date = []
                my_logger.info("visa_dates_extracted: {}".format(dates))
                [issue_date.append(x) for x in dates if x not in issue_date]
                type_of_visa = ' '
                my_logger.info("visa_issuedates_extracted: {}".format(issue_date))
                if (len(issue_date)==2):

                    Date = issue_date[1]
                    parsed_issue=dateparser.parse(Date,settings={'DATE_ORDER': 'DMY'})
                    if parsed_issue==None:
                        Date_of_issue=' '
                    else:
                        issue_join=str((parsed_issue).date())
                        Date_of_issue=issue_join

                else:
                    Date = issue_date[0]
                    parsed_issue=dateparser.parse(Date,settings={'DATE_ORDER': 'DMY'})
                    if parsed_issue==None:
                        Date_of_issue=' '
                    else:
                        issue_join=str((parsed_issue).date())
                        Date_of_issue=issue_join

            entries=['MULTIPLE','SINGLE','DOUBLE']
            entry=' '
            for y in full_text[::-1]:
                result = ''.join(i for i in y if not i.isdigit())
                matched_entries=get_close_matches(result,entries)

                if len(matched_entries)==1:
                    entry=matched_entries[0]
                    #print("no of entries:",matched_entries[0])
            name_with_symbols=(first[5:]).strip('<')
            fullname=name_with_symbols.split('<<')
            surname=re.sub('\ |\?|\.|\!|\/|\;|\:|\<|\>', ' ', fullname[0])
            if (len(fullname)==2):
                mrx=re.sub('\ |\?|\.|\!|\/|\;|\:|\<|\>', ' ', fullname[1])
                givenname=mrx

            else:
                givenname = ''

            visa_number=re.sub(r'[^\w]', ' ',second[0:9])

            nationality=re.sub('\ |\?|\.|\!|\/|\;|\:|\<', ' ', second[10:13])
            birthdate = second[13:19]

            birth_joindate = '/'.join([birthdate[:2],birthdate[2:4],birthdate[4:]])
            parsed_birth=dateparser.parse(birth_joindate,settings={'DATE_ORDER': 'YMD'})
            #print(parsed_birth)
            if parsed_birth==None:
                date_of_birth =''
            else:
                date_of_birth = str((parsed_birth).date())
                year = date_of_birth[0:4]

                present_date=datetime.datetime.now()
                present_year=present_date.year
                if str(present_year)<year:
                    two=date_of_birth[0:2]
                    remain=date_of_birth[2:]
                    full=two.replace(str(20),str(19))
                    date_of_birth=full+remain
            sex=second[20]
            expiry_date=second[21:27]
            expiry_joindate='/'.join([expiry_date[:2],expiry_date[2:4],expiry_date[4:]])
            parsed_expiry=dateparser.parse(expiry_joindate,settings={'DATE_ORDER': 'YMD'})
            if parsed_expiry==None:
                date_of_expiry =' '
            else:
                date_of_expiry = str((parsed_expiry).date())
            if Date_of_issue == date_of_expiry or Date_of_issue == date_of_birth:
                Date_of_issue= ' '
            optional_data=second[28:]
            if Date_of_issue !=' ':
                Date_of_issue=Date_of_issue[8:]+'/'+Date_of_issue[5:7]+'/'+Date_of_issue[0:4]
            if date_of_birth !='':
                date_of_birth=date_of_birth[8:]+'/'+date_of_birth[5:7]+'/'+date_of_birth[0:4]
            if date_of_expiry !=' ':
                date_of_expiry =date_of_expiry[8:]+'/'+date_of_expiry[5:7]+'/'+date_of_expiry[0:4]
            country_code2=' '
            country_long=' '
            nationality_code2=' '
            nationality_long=' '
            for x in codes:
                for key,values in x.items():
                    if values == issuingcountry:
                        country_long = x["Full_Name"]
                        country_code2 = x["ISO_2_Code"]
                        
                    if values == nationality:
                        nationality_long = x["Full_Name"]
                        nationality_code2 = x["ISO_2_Code"]
            if  sex!='M' or sex!='F':
                sex = ' '          
            data={"Document_Type":type,"visa_Type":type_of_visa,"Issued_country":issuingcountry,"Visa_No_Of_Enteries":entry,"FamilyName":surname,"Visa_Issue_Date":Date_of_issue,"Given_Name":givenname,"Visa_Number":visa_number,"Nationality":nationality,"Date_of_Birth":date_of_birth,"Gender":sex,"Visa_Expiry_Date":date_of_expiry}            
            data={"IDType":"Visa","Country":country_code2,"FirstName":surname,"MiddleName":"","LastName":givenname,"IssueDate":Date_of_issue,"PassportNumber":visa_number,"Nationality":nationality_code2,"BirthDate":date_of_birth,"Sex":sex,"ExpirationDate":date_of_expiry,"Address1":" ","Address2":" ","Address3":" ","CountryLong":country_long,"NationalityLong":nationality_long,"PersonalNumber":" ","PlaceOfBirth":" "}
            my_logger.info("details-extracted:{}".format(data))
            details = {"type":"VISA","data":data}
           # print("person_visa_details:",data)
            return data
        else:
            details={"error":"partial data","message":"image not scaned properly"}
            my_logger.warning("details-not-extracted")
            return details

    except Exception as e:
        my_logger.warning(traceback.format_exc())
        return ({"error":str(e)})
        