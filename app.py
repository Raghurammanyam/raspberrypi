from flask import Flask
from flask_restful import Api


app = Flask(__name__)
api=Api(app)

app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

from controller import detailedcam

api.add_resource(detailedcam,'/api/camera')




if __name__ == "__main__":
    #app.url_map.strict_slashes = False
    app.run(host='0.0.0.0',port=5000,debug=True,use_reloader=False)